﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Epam.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<(string topic, string data, string content)> articles = new List<(string topic, string data, string content)>
            {
                ("Business", "25.12.2017", "Some text about that...."),
                ("Sport", "10.10.2020", "Some text about that...."),
                ("Education", "12.12.2020", "Some text about that...."),
                ("Guitar", "13.02.2021", "Some text about that....")
            };

            ViewBag.Articles = articles;
            return View();
        }

    }
}