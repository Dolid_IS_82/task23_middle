﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Epam.Helpers
{
    public static class HtmlHelperExtension
    {
        public static IHtmlString Radio(this HtmlHelper helper, List<string> elements, string name)
        {
            string result = "";
            for (int i = 0; i < elements?.Count; i++)
            {
                result += "<div class=\"custom-control custom-radio\">";
                result += $"<input class=\"custom-control-input\" type=\"radio\" id=\"{name}{i}\"  name=\"{name}\" value=\"{elements[i]}\">";
                result += $" <label class=\"custom-control-label\" for=\"{name}{i}\">{elements[i]}</label>";
                result += "</div>";
            }


            return MvcHtmlString.Create(result);
        }

        public static IHtmlString CheckBox(this HtmlHelper helper, List<string> elements, string name)
        {

            string result = "";
            for (int i = 0; i < elements?.Count; i++)
            {
                result += "<div class=\"custom-control custom-switch\">";
                result += $"<input class=\"custom-control-input\" type=\"checkbox\" id=\"{name}{i}\"  name=\"{name}\" value=\"{elements[i]}\">";
                result += $" <label class=\"custom-control-label\" for=\"{name}{i}\">{elements[i]}</label>";
                result += "</div>";
            }

            return MvcHtmlString.Create(result);
        }
    }
}